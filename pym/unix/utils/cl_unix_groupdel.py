# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.core.server.func import Action, Tasks
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.cl_template import TemplatesError
from calculate.lib.utils.files import FilesError
from calculate.lib.datavars import VariableError
from calculate.server.variables.action import Actions
from ..unix import UnixError, Unix
from calculate.ldap.ldap import LdapError
from calculate.ldap.utils.cl_ldap_setup import MetaTasks

from calculate.server.server import ServerError

_ = lambda x: x
setLocalTranslate('cl_unix3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClUnixGroupdelAction(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError,
                    TemplatesError,
                    VariableError,
                    ServerError,
                    UnixError,
                    LdapError)

    successMessage = __("Group {ur_unix_group_name} deleted from Unix service!")
    failedMessage = __(
        "Failed to delete {ur_unix_group_name} group from Unix server!")
    interruptMessage = __("Deleting of group manually interrupted")

    stop_tasks = [
    ]

    meta_tasks = MetaTasks("Unix")

    # список задач для действия
    tasks = [
        {'name': 'remove_group',
         'method': 'Unix.remove_group(unix.ur_unix_group_name)'
         },
    ]
