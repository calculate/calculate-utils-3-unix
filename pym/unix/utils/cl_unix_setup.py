# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.core.server.func import Action, Tasks
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.cl_template import TemplatesError
from calculate.lib.utils.files import FilesError
from calculate.lib.datavars import VariableError
from calculate.server.variables.action import Actions
from ..unix import UnixError, Unix
from calculate.ldap.ldap import LdapError, Ldap
from calculate.ldap.utils.cl_ldap_setup import MetaTasks

from calculate.ldap.utils.cl_ldap_setup import ClLdapSetupAction

from calculate.server.server import ServerError

_ = lambda x: x
setLocalTranslate('cl_unix3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClUnixSetupAction(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError,
                    TemplatesError,
                    VariableError,
                    ServerError,
                    UnixError,
                    LdapError)

    successMessage = None
    failedMessage = __("Failed to configure Unix server!")
    interruptMessage = __("Unix server configuration manually interrupted")

    stop_tasks = [
    ]

    meta_tasks = MetaTasks("Unix")

    uninstall_tasks = [
        {'name': 'clear_creds',
         'method': 'Server.clear_service_data("unix")'
         },
        {'name': 'unset_ldap',
         'method': 'Server.service_uninstall("unix")'
         },
    ]

    # список задач для действия
    tasks = [
        {'name': 'set_ldif',
         'method': 'Unix.set_ldap_connection('
                   'ldap.ld_admin_dn,ldap.ld_admin_pw)',
         },
        {'name': 'remove_service',
         'condition': lambda Get: (Get('cl_unix_remove_set') == 'on' or
                                   Get('server.sr_unix_set') != 'on')
         },
        {'name': 'remove_only',
         'message': _("Removing Unix service"),
         'condition': lambda Get: (Get('cl_unix_remove_set') == 'on' and
                                   Get('server.sr_unix_set') == 'on')
         },
        {'name': 'setup_service',
         'condition': lambda Get: Get('server.sr_unix_set') != 'on'
         },
        {'name': 'remove_service:remove_old_db',
         'method': 'Unix.remove_ldap_branch(ld_unix_dn)',
         'condition': lambda Get: Get('server.sr_unix_set') == 'on'
         },
        {'name': 'remove_service:uninstall',
         'tasks': uninstall_tasks,
         'condition': lambda Get: Get('server.sr_unix_set') == 'on'
         },
        {'name': 'setup_service:apply_ldif',
         'tasks': meta_tasks.ldif_task("ldap.ld_admin_dn,ldap.ld_admin_pw",
                                       Actions.Setup)
         },
        {'name': 'setup_service:set_unix',
         'method': 'Server.service_install("unix")'
         },
        {'name': 'generate_password',
         'message': _("Create new password for Unix service"),
         'method': 'Unix.generate_password(ld_unix_bind_dn,'
                   'ld_unix_hash,"Unix")',
         'condition': lambda Get: Get('cl_unix_pw_generate_set') == 'on'
         },
        {'name': 'templates',
         'message': __("System configuration"),
         'method': 'Server.applyTemplates(install.cl_source,'
                   'False,True,None,True,True)',
         },
        {'name': 'restart_slapd',
         'message': __("Restarting LDAP service"),
         'method': 'Server.restart_service("%s")' % Ldap.Service.LDAP,
         },
        {'name': 'save_creds',
         'method': 'Server.save_service_data("unix",'
                   'ld_unix_bind_dn,ld_unix_pw)',
         'condition': lambda Get: Get('cl_unix_remove_set') != 'on'
         },
        {'name': 'success',
         'message': __("Unix server configured!"),
         'condition': lambda Get: Get('cl_unix_remove_set') != 'on',
         'depend': (Tasks.success() & Tasks.hasnot("failed"))
         },
        {'name': 'success',
         'message': __("Unix server removed!"),
         'condition': lambda Get: Get('cl_unix_remove_set') == 'on',
         'depend': (Tasks.success() & Tasks.hasnot("failed"))
         }
        # {'name': 'save_data',
        # 'method': 'Unix.save_variables()'
        # }
    ]

    ClLdapSetupAction.register_stop(stop_tasks)
    ClLdapSetupAction.register_uninstall(uninstall_tasks)
