# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.core.server.func import Action, Tasks
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.cl_template import TemplatesError
from calculate.lib.utils.files import FilesError
from calculate.lib.datavars import VariableError
from calculate.server.variables.action import Actions
from ..unix import UnixError, Unix
from calculate.ldap.ldap import LdapError
from calculate.ldap.utils.cl_ldap_setup import MetaTasks

from calculate.server.server import ServerError

_ = lambda x: x
setLocalTranslate('cl_unix3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClUnixGroupmodAction(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError,
                    TemplatesError,
                    VariableError,
                    ServerError,
                    UnixError,
                    LdapError)

    successMessage = __("Group {ur_unix_group_name} modified in Unix service!")
    failedMessage = __("Failed to modify {ur_unix_group_name} group "
                       "in Unix server!")
    interruptMessage = __("Modifing of group manually interrupted")

    stop_tasks = [
    ]

    meta_tasks = MetaTasks("Unix")

    # список задач для действия
    tasks = [
        {'name': 'remove_users',
         'method': 'Unix.remove_users_from_group(unix.ur_unix_group_users_del,'
                   'unix.ur_unix_group_name_exists)',
         'condition': lambda Get: Get('unix.ur_unix_group_users_del')
         },
        {'name': 'append_users',
         'method': 'Unix.add_users_in_group(unix.ur_unix_group_users_add,'
                   'unix.ur_unix_group_name_exists)',
         'condition': lambda Get: Get('unix.ur_unix_group_users_add')
         },
        {'name': 'rename_group',
         'message': _("Renaming group {unix.ur_unix_group_name} to "
                      "{unix.ur_unix_group_newname}"),
         'method': 'Unix.rename_group(unix.ur_unix_group_name,'
                   'unix.ur_unix_group_newname)',
         'condition': lambda Get: (Get('unix.ur_unix_group_newname') !=
                                   Get('unix.ur_unix_group_name'))
         },
        {'name': 'change_comment',
         'message': _("Comment for group {unix.ur_unix_group_name} changed to "
                      "{unix.ur_unix_group_comment}"),
         'method': 'Unix.update_group_comment(unix.ur_unix_group_name,'
                   'unix.ur_unix_group_comment)',
         'condition': lambda Get: Get('unix.ur_unix_group_comment') != Get(
             'unix.ur_unix_group_comment_exists')
         },
        {'name': 'group_change_id',
         'message': _("Changed ID for group {unix.ur_unix_group_name} "
                      "to {unix.ur_unix_group_id}"),
         'method': 'Unix.update_group_id(unix.ur_unix_group_name,'
                   'unix.ur_unix_group_id,unix.ur_unix_group_id_exists)',
         'condition': lambda Get: (Get('unix.ur_unix_group_id') != Get(
             'unix.ur_unix_group_id_exists'))
         },
    ]
