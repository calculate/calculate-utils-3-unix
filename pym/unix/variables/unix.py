# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.lib.datavars import (ReadonlyVariable, Variable,
                                    VariableError)
from calculate.ldap.variables.helpers import (HashHelper, RandomPasswordHelper,
                                              ServerEnvHelper)

_ = lambda x: x
from calculate.lib.cl_lang import (setLocalTranslate, getLazyLocalTranslate)

setLocalTranslate('cl_unix3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class VariableLdUnixLogin(ReadonlyVariable):
    """
    Имя администратор Unix сервера
    """
    value = "Unix"

class VariableLdUnixDn(ReadonlyVariable):
    """
    DN сервиса
    """
    value_format = "ou={ld_unix_login},{ldap.ld_services_dn}"

class VariableLdUnixBindDn(ServerEnvHelper, Variable):
    """
    Пароль root
    """
    service = "unix"
    parameter = "DN"

    @property
    def fallback_value(self):
        return self.Get('ld_unix_dn')


class VariableLdUnixUsersDn(ReadonlyVariable):
    """
    DN пользователей сервиса
    """
    value_format = "ou=Users,{ld_unix_dn}"


class VariableLdUnixGroupsDn(ReadonlyVariable):
    """
    DN групп сервиса
    """
    value_format = "ou=Groups,{ld_unix_dn}"


class VariableLdUnixHash(HashHelper, ReadonlyVariable):
    """
    Хэш рут пароля
    """
    source = "ld_unix_pw"


class VariableLdUnixPw(ServerEnvHelper, RandomPasswordHelper, Variable):
    """
    Пароль root
    """
    password_len = 9
    service = "unix"
    parameter = "PASS"

    @property
    def fallback_value(self):
        return RandomPasswordHelper.get(self)

    def get(self):
        if self.Get('cl_unix_pw_generate_set') == 'on':
            return RandomPasswordHelper.get(self)
        else:
            return super(VariableLdUnixPw, self).get()

class VariableClUnixPwGenerateSet(Variable):
    """
    Перегенерировать пароль или нет
    """
    type = "bool"

    opt = ("-g", "--gen-password")
    value = "off"

    def init(self):
        self.label = _("Generate new service password")
        self.help = _("generate new service password")

    def uncompatible(self):
        if self.Get('server.sr_unix_set') != 'on':
            return "Unavailable for unconfigured Unix service"

class VariableClUnixRemoveSet(Variable):
    """
    Удалить сервис Unix
    """
    type = "bool"
    guitype = "hidden"

    opt = ("-r", "--remove")
    value = "off"

    def init(self):
        self.label = _("Remove service")
        self.help = _("remove service")

    def check(self, value):
        if self.Get('server.sr_unix_set') != 'on':
            raise VariableError(
                _("Unix service has not been configured"))

    def uncompatible(self):
        if self.Get('server.sr_unix_set') != 'on':
            return "Unavailable for unconfigured Unix service"
