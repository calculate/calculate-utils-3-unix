# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.lib.datavars import (VariableError, Variable)

from calculate.lib.cl_lang import setLocalTranslate
from helpers import UnixGroupHelper, UnixUserHelper, Actions

_ = lambda x: x
setLocalTranslate('cl_unix3', sys.modules[__name__])


class VariableClUnixAction(UnixGroupHelper, UnixUserHelper, Variable):
    """
    Дополнительный тип действия для переменной
    """
    value = ""

    def check(self, value):
        if (value not in Actions.All and
                not self.GetBool('server.sr_unix_set')):
            raise VariableError(_("Unix service has not been configured"))
        if value in Actions.UserExists and not self.ldap_user_list():
            raise VariableError(_("Unix service has no users"))
        if value in Actions.GroupExists and not self.ldap_group_list():
            raise VariableError(_("Unix service has no groups"))
        # проверить соединение с ldap
        if value != Actions.Setup:
            self.Get('ldap.cl_ldap_connect')

