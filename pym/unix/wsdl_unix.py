# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys

from calculate.lib.datavars import VariableError, DataVarsError

from calculate.core.server.func import WsdlBase, CustomButton
from .unix import Unix, UnixError
from calculate.ldap.ldap import LdapError
from calculate.unix.variables.action import Actions
from calculate.server.server import Server
from calculate.server.variables.action import Actions as ServerActions
from utils.cl_unix_setup import ClUnixSetupAction
from utils.cl_unix_useradd import ClUnixUseraddAction
from utils.cl_unix_usermod import ClUnixUsermodAction
from utils.cl_unix_userdel import ClUnixUserdelAction
from utils.cl_unix_passwd import ClUnixPasswdAction
from utils.cl_unix_groupadd import ClUnixGroupaddAction
from utils.cl_unix_groupmod import ClUnixGroupmodAction
from utils.cl_unix_groupdel import ClUnixGroupdelAction
from utils.cl_unix_groupshow import ClUnixGroupshowAction
from utils.cl_unix_usershow import ClUnixUsershowAction
from calculate.ldap.ldap import Ldap

_ = lambda x: x
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate

setLocalTranslate('cl_unix3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class Wsdl(WsdlBase):
    methods = [
        #
        # Настроить службу
        #
        {
            # идентификатор метода
            'method_name': Unix.Method.Setup,
            # категория метода
            'category': __('Setup Server'),
            # заголовок метода
            'title': __("Unix Accounts"),
            # иконка для графической консоли
            'image': 'calculate-unix-setup,preferences-system-login,ooo-base,'
                     'office-database',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-unix-setup',
            # права для запуска метода
            'rights': ['unix'],
            # объект содержащий модули для действия
            'logic': {'Unix': Unix,
                      'Server': Server},
            # описание действия
            'action': ClUnixSetupAction,
            # объект переменных
            'datavars': "unix",
            'native_error': (
                VariableError, DataVarsError, LdapError, UnixError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': ServerActions.Setup,
                        'server.cl_server_name': Unix.service_name,
                        'cl_unix_action': Actions.Setup,
                        'cl_autoupdate_set': 'on'
                        #  'cl_dispatch_conf_default': "usenew"
                        },
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("Unix Accounts"),
                    brief=(
                        'ldap.ld_base_dn',
                        'server.sr_ldap_set',
                        'server.sr_unix_set',
                    ),
                    expert=('cl_unix_pw_generate_set',
                            'cl_unix_remove_set',
                            'cl_verbose_set',),
                    hide=(),
                    custom_buttons=[
                        CustomButton.run_method(
                            Unix.Method.Setup, "but0", _("Remove"),
                            CustomButton.Behavior.setvalue(
                                'cl_unix_remove_set', 'on'),
                            lambda Get: Get('server.sr_unix_set') == 'on'),
                        CustomButton.next_button("but1")
                    ]
                ),
            ],
            'depends': [Ldap.Method.Setup],
            'brief': {'next': __("Run"),
                      'name': __("Setup Accounts Server"), }
        },
        #
        # Добавить пользователя
        #
        {
            # идентификатор метода
            'method_name': Unix.Method.UserAdd,
            # заголовок метода
            'title': __("Add User"),
            # иконка для графической консоли
            'image': None,
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-unix-useradd',
            # права для запуска метода
            'rights': ['unix'],
            # объект содержащий модули для действия
            'logic': {'Unix': Unix,
                      'Server': Server},
            # описание действия
            'action': ClUnixUseraddAction,
            # объект переменных
            'datavars': "unix",
            'native_error': (
                VariableError, DataVarsError, LdapError, UnixError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': ServerActions.Setup,
                        'server.cl_server_name': Unix.service_name,
                        'cl_unix_action': Actions.UserAdd,
                        'cl_autoupdate_set': 'on',
                        'ldap.cl_ldap_bind_dn!': '{ld_unix_bind_dn}',
                        'ldap.cl_ldap_bind_pw!': '{ld_unix_pw}',
                        #  'cl_dispatch_conf_default': "usenew"
                        },
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("User"),
                    brief=('ur_unix_login',),
                    normal=(
                        'ur_unix_login',
                        'ur_unix_pw',
                        'ur_unix_comment',
                        'ur_unix_primary_group',
                        'ur_unix_groups',
                    ),
                    expert=(
                        'ur_unix_uid',
                        'ur_unix_home_path',
                        'ur_unix_shell',
                        'ur_unix_create_home_set',
                        'ur_unix_visible_set',
                        'ur_unix_skel',
                    ),
                    next_label=_("Run")
                ),
            ],
            'depends': [Unix.Method.Setup],
        },
        #
        # Добавить группу
        #
        {
            # идентификатор метода
            'method_name': Unix.Method.GroupAdd,
            # заголовок метода
            'title': __("Add Group"),
            # иконка для графической консоли
            'image': None,
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-unix-groupadd',
            # права для запуска метода
            'rights': ['unix'],
            # объект содержащий модули для действия
            'logic': {'Unix': Unix,
                      'Server': Server},
            # описание действия
            'action': ClUnixGroupaddAction,
            # объект переменных
            'datavars': "unix",
            'native_error': (
                VariableError, DataVarsError, LdapError, UnixError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': ServerActions.Setup,
                        'server.cl_server_name': Unix.service_name,
                        'cl_unix_action': Actions.GroupAdd,
                        'cl_autoupdate_set': 'on',
                        'ldap.cl_ldap_bind_dn!': '{ld_unix_bind_dn}',
                        'ldap.cl_ldap_bind_pw!': '{ld_unix_pw}',
                        #  'cl_dispatch_conf_default': "usenew"
                        },
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("Add New Group"),
                    brief=(),
                    hide=('ur_unix_base_dir',),
                    normal=(
                        'ur_unix_group_name',
                        'ur_unix_group_id',
                        'ur_unix_group_comment',
                    ),
                    expert=(),
                    next_label=_("Run")
                ),
            ],
            'depends': [Unix.Method.Setup],
        },
        #
        # Изменить параметры группы
        #
        {
            # идентификатор метода
            'method_name': Unix.Method.GroupMod,
            # заголовок метода
            'title': __("Modify Group"),
            # иконка для графической консоли
            'image': None,
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-unix-groupmod',
            # права для запуска метода
            'rights': ['unix'],
            # объект содержащий модули для действия
            'logic': {'Unix': Unix,
                      'Server': Server},
            # описание действия
            'action': ClUnixGroupmodAction,
            # объект переменных
            'datavars': "unix",
            'native_error': (
                VariableError, DataVarsError, LdapError, UnixError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': ServerActions.Setup,
                        'server.cl_server_name': Unix.service_name,
                        'cl_unix_action': Actions.GroupMod,
                        'cl_autoupdate_set': 'on',
                        'ldap.cl_ldap_bind_dn!': '{ld_unix_bind_dn}',
                        'ldap.cl_ldap_bind_pw!': '{ld_unix_pw}',
                        #  'cl_dispatch_conf_default': "usenew"
                        },
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("Group"),
                    brief=('ur_unix_group_name_exists',),
                    normal=(
                        'ur_unix_group_name_exists',
                        'ur_unix_group_newname',
                        'ur_unix_group_id',
                        'ur_unix_group_comment',
                        'ur_unix_group_users_replace',
                    ),
                    hide=('ur_unix_group_users_replace',),
                    expert=(
                        'ur_unix_group_users_add',
                        'ur_unix_group_users_del',
                    ),
                    custom_buttons=[
                        CustomButton.run_method(
                            Unix.Method.GroupShow, 'but0', _("Back")),
                        CustomButton.run_method(
                            Unix.Method.GroupDel, 'but1', _("Delete"),
                            CustomButton.Behavior.linkerror(
                                source='ur_unix_group_name_exists',
                                target='ur_unix_group_newname')),
                        CustomButton.run_method(
                            Unix.Method.GroupMod, 'but2', _("Modify")),
                    ]
                ),
            ],
            'invalidators': {
                'ur_unix_group_name_exists': (
                    'ur_unix_group_id',
                    'ur_unix_group_comment',
                    'ur_unix_group_newname',
                    'ur_unix_group_users_replace',
                    'ur_unix_group_users_add',
                    'ur_unix_group_users_del',)
            },
            'depends': [Unix.Method.Setup],
        },
        #
        # Удалить группу
        #
        {
            # идентификатор метода
            'method_name': Unix.Method.GroupDel,
            # заголовок метода
            'title': __("Delete Group"),
            # иконка для графической консоли
            'image': None,
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-unix-groupdel',
            # права для запуска метода
            'rights': ['unix'],
            # объект содержащий модули для действия
            'logic': {'Unix': Unix,
                      'Server': Server},
            # описание действия
            'action': ClUnixGroupdelAction,
            # объект переменных
            'datavars': "unix",
            'native_error': (
                VariableError, DataVarsError, LdapError, UnixError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': ServerActions.Setup,
                        'server.cl_server_name': Unix.service_name,
                        'cl_unix_action': Actions.GroupDel,
                        'cl_autoupdate_set': 'on',
                        'ldap.cl_ldap_bind_dn!': '{ld_unix_bind_dn}',
                        'ldap.cl_ldap_bind_pw!': '{ld_unix_pw}',
                        #  'cl_dispatch_conf_default': "usenew"
                        },
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("Group"),
                    brief=(),
                    normal=(
                        'ur_unix_group_name_exists',
                    ),
                    next_label=_("Run")
                ),
            ],
            'depends': [Unix.Method.Setup]
        },
        #
        # Изменить параметры пользователя
        #
        {
            # идентификатор метода
            'method_name': Unix.Method.UserMod,
            # заголовок метода
            'title': __("Modify User"),
            # иконка для графической консоли
            'image': None,
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-unix-usermod',
            # права для запуска метода
            'rights': ['unix'],
            # объект содержащий модули для действия
            'logic': {'Unix': Unix,
                      'Server': Server},
            # описание действия
            'action': ClUnixUsermodAction,
            # объект переменных
            'datavars': "unix",
            'native_error': (
                VariableError, DataVarsError, LdapError, UnixError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': ServerActions.Setup,
                        'server.cl_server_name': Unix.service_name,
                        'cl_unix_action': Actions.UserMod,
                        'cl_autoupdate_set': 'on',
                        'ldap.cl_ldap_bind_dn!': '{ld_unix_bind_dn}',
                        'ldap.cl_ldap_bind_pw!': '{ld_unix_pw}',
                        #  'cl_dispatch_conf_default': "usenew"
                        },
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("User"),
                    normal=(
                        'ur_unix_login_exists',
                        'ur_unix_pw',
                        'ur_unix_primary_group',
                        'ur_unix_comment',
                        'ur_unix_groups',
                    ),
                    expert=(
                        'ur_unix_groups_add',
                        'ur_unix_groups_del',
                        'ur_unix_lock_set',
                        'ur_unix_pw_delete_set',
                        'ur_unix_visible_set',
                        'ur_unix_home_path',
                        'ur_unix_home_path_move',
                        'ur_unix_shell',
                    ),
                    custom_buttons=[
                        CustomButton.run_method(Unix.Method.UserShow,
                                                'but0', _("Back")),
                        CustomButton.run_method(Unix.Method.UserDel,
                                                'but1', _("Delete")),
                        CustomButton.run_method(Unix.Method.UserMod,
                                                'but2', _("Modify"))
                    ],
                    next_label=_("Run")
                ),
            ],
            'invalidators': {
                'ur_unix_login_exists': (
                    'ur_unix_primary_group',
                    'ur_unix_comment',
                    'ur_unix_groups',
                    'ur_unix_visible_set',
                    'ur_unix_lock_set',
                    'ur_unix_groups_add',
                    'ur_unix_groups_del',
                    'ur_unix_home_path',
                    'ur_unix_home_path_move',
                    'ur_unix_shell',
                    'ur_unix_pw_delete_set',
                )
            },
            'depends': [Unix.Method.Setup]
        },
        #
        # Удалить пользователя
        #
        {
            # идентификатор метода
            'method_name': Unix.Method.UserDel,
            # заголовок метода
            'title': __("Delete User"),
            # иконка для графической консоли
            'image': None,
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-unix-userdel',
            # права для запуска метода
            'rights': ['unix'],
            # объект содержащий модули для действия
            'logic': {'Unix': Unix,
                      'Server': Server},
            # описание действия
            'action': ClUnixUserdelAction,
            # объект переменных
            'datavars': "unix",
            'native_error': (
                VariableError, DataVarsError, LdapError, UnixError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': ServerActions.Setup,
                        'server.cl_server_name': Unix.service_name,
                        'cl_unix_action': Actions.UserDel,
                        'cl_autoupdate_set': 'on',
                        'ldap.cl_ldap_bind_dn!': '{ld_unix_bind_dn}',
                        'ldap.cl_ldap_bind_pw!': '{ld_unix_pw}',
                        #  'cl_dispatch_conf_default': "usenew"
                        },
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("User"),
                    brief=(),
                    normal=(
                        'ur_unix_login_exists',
                    ),
                    next_label=_("Run")
                ),
            ],
            'depends': [Unix.Method.Setup]
        },
        #
        # Сменить пароль пользователю
        #
        {
            # идентификатор метода
            'method_name': Unix.Method.Passwd,
            # заголовок метода
            'title': __("Password Modification"),
            # иконка для графической консоли
            'image': None,
            # метод присутствует в графической консоли
            'gui': False,
            # консольная команда
            'command': 'cl-unix-passwd',
            # права для запуска метода
            'rights': ['unix'],
            # объект содержащий модули для действия
            'logic': {'Unix': Unix,
                      'Server': Server},
            # описание действия
            'action': ClUnixPasswdAction,
            # объект переменных
            'datavars': "unix",
            'native_error': (
                VariableError, DataVarsError, LdapError, UnixError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': ServerActions.Setup,
                        'server.cl_server_name': Unix.service_name,
                        'cl_unix_action': Actions.Passwd,
                        'cl_autoupdate_set': 'on',
                        'ldap.cl_ldap_bind_dn!': '{ld_unix_bind_dn}',
                        'ldap.cl_ldap_bind_pw!': '{ld_unix_pw}',
                        #  'cl_dispatch_conf_default': "usenew"
                        },
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("User"),
                    brief=(),
                    normal=(
                        'ur_unix_login_exists',
                        'ur_unix_pw',
                        'ur_unix_pw_delete_set',
                        'ur_unix_lock_set'
                    ),
                    next_label=_("Run")
                ),
            ],
            'depends': [Unix.Method.Setup]
        },
        #
        # Просмотр групп
        #
        {
            # идентификатор метода
            'method_name': Unix.Method.GroupShow,
            # категория метода
            'category': __('Accounts Server'),
            # заголовок метода
            'title': __("Unix Groups"),
            # иконка для графической консоли
            'image': 'calculate-unix-groupshow,group,system-users',
            # метод присутствует в графической консоли
            'gui': lambda Get: Get('server.sr_unix_set') == 'on',
            # консольная команда
            'command': 'cl-unix-groupshow',
            # права для запуска метода
            'rights': ['unix'],
            # объект содержащий модули для действия
            'logic': {'Unix': Unix,
                      'Server': Server},
            # описание действия
            'action': ClUnixGroupshowAction,
            # объект переменных
            'datavars': "unix",
            'native_error': (
                VariableError, DataVarsError, LdapError, UnixError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': ServerActions.Setup,
                        'server.cl_server_name': Unix.service_name,
                        'cl_unix_action': Actions.GroupShow,
                        'cl_autoupdate_set': 'on',
                        'core.cl_page_max!': '{ur_unix_group_count}',
                        'ldap.cl_ldap_bind_dn!': '{ld_unix_bind_dn}',
                        'ldap.cl_ldap_bind_pw!': '{ld_unix_pw}',
                        #  'cl_dispatch_conf_default': "usenew"
                        },
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("Groups"),
                    brief=(),
                    next_label=_("Show"),
                    normal=(
                        'ur_unix_group_show',
                        'core.cl_page_count', 'core.cl_page_offset',
                    ),
                    expert=(
                        'cl_unix_group_filter_name',
                        'cl_unix_group_filter_id',
                        'cl_unix_group_filter_comment',
                        'cl_unix_group_filter_users',
                        'cl_unix_group_fields',
                    ),
                    custom_buttons=[
                        CustomButton.open_method(Unix.Method.GroupAdd,
                                                 'but0', _("Add Group")),
                        CustomButton.run_method(Unix.Method.GroupShow,
                                                'but1', _("Show"))
                    ]
                ),
            ],
            'depends': [Unix.Method.Setup]
        },
        #
        # Просмотр пользователей
        #
        {
            # идентификатор метода
            'method_name': Unix.Method.UserShow,
            # категория метода
            'category': __('Accounts Server'),
            # заголовок метода
            'title': __("Unix Users"),
            # иконка для графической консоли
            'image': 'calculate-unix-usershow,user,stock_person,system-users',
            # метод присутствует в графической консоли
            'gui': lambda Get: Get('server.sr_unix_set') == 'on',
            # консольная команда
            'command': 'cl-unix-usershow',
            # права для запуска метода
            'rights': ['unix'],
            # объект содержащий модули для действия
            'logic': {'Unix': Unix,
                      'Server': Server},
            # описание действия
            'action': ClUnixUsershowAction,
            # объект переменных
            'datavars': "unix",
            'native_error': (
                VariableError, DataVarsError, LdapError, UnixError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': ServerActions.Setup,
                        'server.cl_server_name': Unix.service_name,
                        'cl_unix_action': Actions.UserShow,
                        'cl_autoupdate_set': 'on',
                        'core.cl_page_max!': '{ur_unix_user_count}',
                        'ldap.cl_ldap_bind_dn!': '{ld_unix_bind_dn}',
                        'ldap.cl_ldap_bind_pw!': '{ld_unix_pw}',
                        #  'cl_dispatch_conf_default': "usenew"
                        },
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("Users"),
                    brief=(),
                    next_label=_("Show"),
                    normal=(
                        'ur_unix_user_show',
                        'core.cl_page_count', 'core.cl_page_offset',
                    ),
                    expert=(
                        'cl_unix_user_filter_login',
                        'cl_unix_user_filter_pw_set',
                        'cl_unix_user_filter_comment',
                        'cl_unix_user_filter_uid',
                        'cl_unix_user_filter_gid',
                        'cl_unix_user_filter_groups',
                        'cl_unix_user_filter_home_path',
                        'cl_unix_user_filter_shell',
                        'cl_unix_user_filter_visible_set',
                        'cl_unix_user_filter_lock_set',
                        'cl_unix_user_fields',
                    ),
                    custom_buttons=[
                        CustomButton.open_method(Unix.Method.UserAdd,
                                                 'but0', _("Add User")),
                        CustomButton.run_method(Unix.Method.UserShow,
                                                'but1', _("Show")),
                    ]
                ),
            ],
            'depends': [Unix.Method.Setup]
        },
    ]
